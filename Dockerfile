FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > gnat.log'

COPY gnat .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' gnat
RUN bash ./docker.sh

RUN rm --force --recursive gnat
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD gnat
